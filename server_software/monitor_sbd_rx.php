<?php
	function hex2str($hex) {
		$str = "";
		for($i=0;$i<strlen($hex);$i+=2)
		   $str .= chr(hexdec(substr($hex,$i,2)));

		return $str;
	}

	include ('ewb_settings.php');

	if (isset($_POST["imei"]) and isset($_POST["momsn"]) and isset($_POST["transmit_time"]) and isset($_POST["iridium_latitude"]) and isset($_POST["iridium_longitude"]) and isset($_POST["iridium_cep"]) and isset($_POST["data"]))
	{
		$recv = (int) time(); // Time since unix epoch, no timezone (or 'UTC')
		$imei = $_POST["imei"];
		$seq = $_POST["momsn"];
		$tx_time = $_POST["transmit_time"];
		$lat = $_POST["iridium_latitude"];
		$lon = $_POST["iridium_longitude"];
		$accuracy = (int) $_POST["iridium_cep"] * 1000; # [m]
		$data = $_POST["data"];

		$tx_time_decoded = strtotime($tx_time." UTC");
		$data_decoded = hex2str($data);

		// convert lat/lon to semicircles (2^32/360)
		$degs_to_semicircles = 11930464.7111;
		$lat_semi = (int) ($lat * $degs_to_semicircles);
		$lon_semi = (int) ($lon * $degs_to_semicircles);

		// Create connection
		$conn = new mysqli($EWB_SERVER, $EWB_USER, $EWB_PASS, $EWB_DB);

		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		// Attempt to look up monitor ID based on imei number
		$sql = "SELECT * FROM $EWB_TABLE_MONITOR_LIST WHERE sender=$imei";
		$result = $conn->query($sql);

		if ($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$id = $row["id"];
		}
		else
		{
			$id = 0;
		}

		// Attempt insert entry
		$sql = "INSERT INTO $EWB_TABLE_MONITOR (id, recv, sent, seq, sender, lat, lon, accuracy, data) VALUES ($id, $recv, $tx_time_decoded, $seq, '$imei', $lat_semi, $lon_semi, $accuracy, '$data_decoded')";

		if ($conn->query($sql) === TRUE)
		{
				echo "OK:", $sql;
		} 
		else 
		{
				echo "Returned: ", $conn->error;
				echo "ERROR: " . $sql;
		}

		// close the connection
		$conn->close();

		// return ok to the requester
		echo "OK";

	// if the request was a user just requesting the page
	} else
	{
		echo "Error";
		die();
	}
?>



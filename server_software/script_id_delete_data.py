#!/usr/bin/python

# sudo apt install python-mysql.connector
import mysql.connector
from ewb_settings import *

delete_id = 1000

cnx = mysql.connector.connect(user=EWB_USER, database=EWB_DB, password=EWB_PASS, port=EWB_PORT)
cursor = cnx.cursor()
query = "DELETE FROM monitor WHERE id = %s" # remember always use %s
cursor.execute(query, (delete_id,))
cnx.commit()
cursor.close()
cnx.close()




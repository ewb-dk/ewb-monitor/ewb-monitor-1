#!/usr/bin/python

monitor_id = 1000

# sudo apt install python-mysql.connector
import mysql.connector
from ewb_settings import *
from script_monitor_list import *

def add_monitor(mon):
	lat_semi = int(mon['lat'] * LL2SEMICIRC + 0.5)
	lon_semi = int(mon['lon'] * LL2SEMICIRC + 0.5)
	sql = 'INSERT INTO %s (id, type, sender, value, scale, unit, name, location, lat, lon, time_zone, owner, pin, report_daily, report_weekly) VALUES (%ld, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%ld\', \'%ld\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')' % (EWB_TABLE_MONITOR_LIST, mon['id'], mon['type'], mon['sender'], mon['value'], mon['scale'], mon['unit'], mon['name'], mon['location'], lat_semi, lon_semi, mon['time_zone'], mon['owner'], mon['pin'], mon['report_daily'], mon['report_weekly'])
	print sql
	cnx = mysql.connector.connect(user=EWB_USER, database=EWB_DB, password=EWB_PASS, port=EWB_PORT)
	cursor = cnx.cursor()
	cursor.execute(sql)
	cnx.commit()
	cursor.close()
	cnx.close()

for i in range(len(monitor_list)):
	if monitor_list[i]['id'] == monitor_id:
		add_monitor (monitor_list[i])


/***************************************************************************
# EWB Monitor (MONITOR) Firmware
# Copyright (c) 2018-2019, Kjeld Jensen <kj@iug.dk> <kj@kjen.dk>
# Engineers Without Borders - Denmark http://iug.dk
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the names of its
#      contributors may be used to endorse or promote products derived from
#      this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#****************************************************************************
# Revision
# 2018-09-03 KJ First released version
# 2018-12-05 KJ Added support for an internal temperature sensor on the
#               one-wire bus.
# 2019-04-03 KJ Fixed a bug causing continuous measurements of air temperature
#               and battery voltage. Source code cleanup. Added DIP switch
#               support. Added soft serial support for Iridium module. Added
#               serial debug support. Reduced current consumption by only
#               initializing the hw serial port prior to use and close it
#               afterwards. Added 15ms idle in each loop to save further
#               power. Modified LED blink. 
#              .
#****************************************************************************

# Board: "Arduino Pro or Pro Mini"
# Processor: "ATmega328P (3.3V, 8 MHz)"

# Remember to copy SBDLib to the Arduino/libraries directory before compiling.

# If using the hardware serial port on PCB 03-2018 remember to disconnect
# wires before programming.

# MONITOR DIP Switch 1
#   ON:  Sends a message each 8h resulting in a total of 4 messages in 24h
#   OFF: Sends 1 message after 24h
#
#***************************************************************************/
/* settings and parameters */

#define FW_VERSION "FW 2019-04-03" 

/* PCB type */
#define PCB_03_2018 /* PCB_03_2018 or outcomment for Arduino Pro mini prototype */

/* send debug info via serial port */
#define DEBUG_ENABLE /* DEBUG_ENABLE or undefined if debug is disabled */
/* #define DEBUG_SOFTWARE_SERIAL */ /* DEBUG_SOFTWARE_SERIAL or undefined for hw serial port*/

/* uplink */
#define UPLINK_SOFTWARE_SERIAL /* UPLINK_SOFTWARE_SERIAL or undefined for hw serial port*/
#define UPLINK_IRIDIUM_ROCKBLOCK /* UPLINK_IRIDIUM_ROCKBLOCK/UPLINK_GSM_SIM800 */

/* iridium module parameters */
#ifdef UPLINK_IRIDIUM_ROCKBLOCK
  #define UPLINK_POWER_ON_TIME 5 /* [s] */
  #define UPLINK_TX_INTERVAL (24 * 3600UL) /* [s] 24 hours */
  #define UPLINK_TX_INTERVAL_DEBUG (8 * 3600UL) /* [s] 8 hours */
  #define UPLINK_TX_MAX_TIME (6 * 60) /* [s] must be smaller than TX_INTERVAL's */
#endif

/* gsm sim800 modem parameters */
#ifdef UPLINK_GSM_SIM800
#endif

/* vfs flowsensor parameters */
#define VFS_5_100 /* VFS_2_40/VFS_5_100 */
#define VFS_MEAS_INTERVAL 250 /* interval between measurements [ms] */

/* monitor parameters */
#define MONITOR_RST_TOUT (24 * 3600UL - 1) /* [s] 24h (watchdog sleeps for up to 1s) */
/* #define MONITOR_RST_TOUT (8 * 3600UL - 1) */ /* [s] TEST (watchdog sleeps for up to 1s) */

/***************************************************************************/
/* includes, defines and global variables */

/* system */
unsigned long time_ms; /* time since boot [ms] */
unsigned long time_s; /* time since boot [s] */

/***************************************************************************/
/* send debug info via serial port */
#ifdef DEBUG_ENABLE
  extern "C"{
    char *debug_serial_s;
  }

  #ifdef DEBUG_SOFTWARE_SERIAL
    #define DEBUG_RX false /*  */
    #define DEBUG_TX false /*  */
  
    #include <SoftwareSerial.h>
    SoftwareSerial debugSS = SoftwareSerial(DEBUG_RX, DEBUG_TX);
    SoftwareSerial *debugSerial = &debugSS;
  #else
    HardwareSerial *debugSerial = &Serial;
  #endif
#endif
/***************************************************************************/
/* low power */
#include "LowPower.h" // https://github.com/rocketscream/Low-Power

/***************************************************************************/
/* led */
#define LED_HEARTBEAT 1 /* weak blink (1ms) */
#define LED_SIGNAL 100 /* powerful blink (100ms) */
#define LED_BLINK_INTERVAL 15000 /* 15s */

unsigned long led_next_blink; /* blink led next time [ms] */

/***************************************************************************/
/* dip switch */

#define DIP_SW_1 3 /* (PD3) DIP switch 1 input port */
#define DIP_SW_2 4 /* (PD4) DIP switch 2 input port */

char dip_sw_1, dip_sw_2, dip_sw_temp;

/***************************************************************************/
/* monitor reset */
#define PIN_MON_RST_REQ A4 // (PC4/SDA) connected to WATCHDOG (PC4/SDA)

/***************************************************************************/
/* uplink parameters */

#ifdef UPLINK_SOFTWARE_SERIAL
  #define UPLINK_RX 12 /* 12 (MISO) Pin 1 in ICSP connector */
  #define UPLINK_TX 11 /* 11 (MOSI) Pin 4 in ICSP connector */

  #include <SoftwareSerial.h>
  SoftwareSerial uplinkSS = SoftwareSerial(UPLINK_RX, UPLINK_TX);
  SoftwareSerial *uplinkSerial = &uplinkSS;
#else
  HardwareSerial *uplinkSerial = &Serial;
#endif

#define PIN_UPLINK_SLEEP 8

char uplink_state;
unsigned long uplink_tx_interval; /* time between uplink transmissions [s] */
unsigned long uplink_tx_next; /* time for next transmission start [s] */
unsigned long uplink_tx_start; /* time since current transmission start [s] */
unsigned char uplink_tx_since_boot; /* number of transmissions since boot */

/***************************************************************************/
/* iridium rockblock module */
#ifdef UPLINK_IRIDIUM_ROCKBLOCK

  #define UPLINK_STATE_IDLE 0
  #define UPLINK_STATE_INIT 1
  #define UPLINK_STATE_SEND 2
  #define UPLINK_STATE_SEND_WAIT 3

  extern "C"{
    #include <SBDLib.h>
  };

  char rssi;
  char return_code;
  char i;
  char msg[50];
  char msg_len;
#endif

/***************************************************************************/
/* gsm sim800 module */
#ifdef UPLINK_GSM_SIM800
#endif

/***************************************************************************/
/* voltage measurement */
#define VOLT_MEAS_INTERVAL 60000

unsigned char batt_v, batt_v_min; // battery voltage [V*10]
unsigned long next_volt_meas;

/***************************************************************************/
/* temperature measurement */
#include <DallasTemperature.h>
#define PIN_ONE_WIRE_BUS A3
OneWire oneWire(PIN_ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
#define TEMP_MEAS_INTERVAL 60000
#define TEMP_MIN_RESET 1000
#define TEMP_MAX_RESET -1000

short temp, temp1_min, temp1_max, temp2_min, temp2_max; // temperature [degrees Celcius*10]
unsigned long next_temp_meas;

/***************************************************************************/
/* vfs flowmeter */
#define PIN_VFS_PWR 7 // (PD7)
#define VFS_FLOW_ADC_THRESHOLD_LOW 102  /* adc value corresponding to 0.5 Volt */
#define VFS_FLOW_ADC_THRESHOLD_HIGH 717  /* adc value corresponding to 3.5 Volt */

        /* VFS 2-40 l/m */
        /* calculation error is approx 0.05l/m at 40 l/m */
        /* 2l/min = 102.4 [adc], 40l/min = 716.8 [adc]
           a = (40-2)*1e6/(716.8-102.4) = +61849
           b = 2000000 - 61849*102.4 = -4333338 */
#ifdef VFS_2_40
#define VFS_FLOW_A 61849
#define VFS_FLOW_B -4333338
#endif

        /* VFS 5-100 l/m */
        /* calculation error is approx 0.14l/m at 100 l/m */
        /* 5l/min = 102.4 [adc], 100l/min = 716.8 [adc]
           a = (100-5)*1e6/(716.8-102.4) = +154622
           b = 5000000 - 154622*102.4 = -10833293 */
#ifdef VFS_5_100
#define VFS_FLOW_A 154622
#define VFS_FLOW_B -10833293
#endif

        /* VFS temperature */
        /* 0 deg = 102.4 [adc], 100 deg = 716.8 [adc]
           a = (100-0)*1e6/(716.8-102.4) = +162760
           b =  0 - 162760*102.4 = -16666667 */
#define VFS_TEMP_A 162920
#define VFS_TEMP_B -16666667

#define VFS_MEAS_PER_MIN (60000 / VFS_MEAS_INTERVAL) /* # measurements/min */

unsigned short vfs_flow_adc; /* water flow [adc/min] */
unsigned long vfs_flow_ul_m; /* water flow [microliter/min] */
unsigned long vfs_vol_meas_ul; /* water volumen for this measurement [microliter] */
unsigned long vfs_vol_ul; /* water volumen [microliter] */
unsigned long vfs_vol_l; /* water volumen [liter] */
unsigned short vfs_temp_adc; /* water temperature [adc] */
unsigned long vfs_temp_deg; /* water temperature [degrees*10] */
unsigned short vfs_temp1_min; /* lowest measured water temperature [degrees*10] */
unsigned long time_vfs_prev_meas;  /* time since boot to previous measurement [ms] */
unsigned long time_prev_status;  /* time since boot to previous status update [ms] */

/***************************************************************************/
void debug_init(void)
{
  #ifdef DEBUG_ENABLE
    debug_serial_s = malloc (100);
  #endif
}
/***************************************************************************/
void debug_serial_tx(char *text)
{
  #ifdef DEBUG_ENABLE
    debugSerial->begin(57600);
    debugSerial->print (time_s);
    debugSerial->print (" ");
    debugSerial->println(text);
    debugSerial->end();
  #endif
}
/***************************************************************************/
void led_init(void)
{
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  led_next_blink = millis() + LED_BLINK_INTERVAL;
}
/***************************************************************************/
void led_blink(char n, char ms)
{
  for (i=0; i<n; i++)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay (ms);
    digitalWrite(LED_BUILTIN, LOW);
    if (i<n-1)
      delay (300);
  }
  led_next_blink += LED_BLINK_INTERVAL;
}
/***************************************************************************/
void led_update(void)
{
  if (time_ms >= led_next_blink)
  {
  if (uplink_state == UPLINK_STATE_IDLE)
    led_blink (1, LED_HEARTBEAT);
  else
    led_blink (2, LED_HEARTBEAT);
  }
}
/***************************************************************************/
void dip_switch_init(void)
{
  pinMode(DIP_SW_1,INPUT_PULLUP);
  pinMode(DIP_SW_2,INPUT_PULLUP);

  dip_sw_1 = -1; /* enforce debug info regardles of switch position */
  dip_sw_2 = -1;
  dip_switch_update(); /* read dip switch positions  */
}
/***************************************************************************/
void dip_switch_update(void)
{
  /* read dip switch 1 */
  dip_sw_temp = !(digitalRead(DIP_SW_1));
  if (dip_sw_temp != dip_sw_1)
  {
    dip_sw_1 = dip_sw_temp;

    /* handle dip switch 1 */
    if (dip_sw_1 == 0)
      uplink_tx_interval = UPLINK_TX_INTERVAL;
    else
      uplink_tx_interval = UPLINK_TX_INTERVAL_DEBUG;

    #ifdef DEBUG_ENABLE
      sprintf (debug_serial_s, "Switch1 %d", dip_sw_1);
      debug_serial_tx (debug_serial_s);
    #endif    
  }

  /* read dip switch 2 */
  dip_sw_temp = !(digitalRead(DIP_SW_2));
  if (dip_sw_temp != dip_sw_2)
  {
    dip_sw_2 = dip_sw_temp;

    #ifdef DEBUG_ENABLE
      sprintf (debug_serial_s, "Switch2 %d", dip_sw_2);
      debug_serial_tx (debug_serial_s);
    #endif    
  }

  /* handle dip switch 2 */

}
/***************************************************************************/
void monitor_reset_init(void)
{
  pinMode(PIN_MON_RST_REQ, OUTPUT);
  digitalWrite(PIN_MON_RST_REQ, HIGH);
}
/***************************************************************************/
void monitor_reset_update(void)
{
  if (time_s >= MONITOR_RST_TOUT)
  {
    debug_serial_tx ("Reset");
    digitalWrite(PIN_MON_RST_REQ, LOW);
  }
}
/***************************************************************************/
void volt_init(void)
{
  batt_v_min = 255;
  next_volt_meas = millis() / 1000;
}
/***************************************************************************/
void volt_update(void)
{
  if (time_ms >= next_volt_meas)
  {
    // see ewb_mon1_batt_volt.ino for documentation
    #ifdef PCB_03_2018
      batt_v = (((analogRead(A0) * 1500150L) >> 10) + 5000) / 10000;
    #else
      batt_v = (((analogRead(A0) * 1719950L) >> 10) + 5000) / 10000;
    #endif

    #ifdef DEBUG_ENABLE
      sprintf (debug_serial_s, "Batt: %d", batt_v);
      debug_serial_tx (debug_serial_s);
    #endif

    if (batt_v_min > batt_v)
      batt_v_min = batt_v;

    next_volt_meas += VOLT_MEAS_INTERVAL;
  }
}
/***************************************************************************/
void uplink_tx_update(void)
{
  if (time_s >= uplink_tx_next + uplink_tx_interval - UPLINK_TX_MAX_TIME)
  {
    while (uplink_tx_next + uplink_tx_interval - UPLINK_TX_MAX_TIME < time_s) /* ensure we are up to date */
      uplink_tx_next += uplink_tx_interval;
      
    uplink_tx();
  }
}

#ifdef UPLINK_IRIDIUM_ROCKBLOCK
/***************************************************************************/
void uplink_init(void)
{
  uplink_power (false); /* put the RockBlock in sleep mode (while still conf. as input) */
  pinMode(PIN_UPLINK_SLEEP, OUTPUT); /* configure the RockBlock pin as output */
  uplink_state = UPLINK_STATE_IDLE;
  uplink_tx_since_boot = 0;

  /* send reset msg if in debug mode */
  if (dip_sw_1 == 1) 
    uplink_tx();
}
/***************************************************************************/
void uplink_power (char on)
{
  if (on)
  {
    digitalWrite(PIN_UPLINK_SLEEP, HIGH); /* turn on the power */
    uplinkSerial->begin(19200); /* init the serial port */
  }
  else
  {
    uplinkSerial->end(); /* close the serial port */
    digitalWrite(PIN_UPLINK_SLEEP, LOW); /* turn off the power */
  }
}
/***************************************************************************/
void uplink_tx()
{
  debug_serial_tx ("Uplink: Start");
  uplink_state = UPLINK_STATE_INIT; 
}
/***************************************************************************/
void uplink_update()
{
  /* if time to send update then send it */
  uplink_tx_update();

  // if any serial data available
  while (uplinkSerial->available() > 0)
    sbd_serial_rx_byte(uplinkSerial->read());

  // state machine
  switch (uplink_state)
  {
    case UPLINK_STATE_IDLE:
      break;

    case UPLINK_STATE_INIT:
      uplink_tx_start = time_s;

     /* power up the iridium module */
      uplink_power (true);
      uplink_state = UPLINK_STATE_SEND;
 
    case UPLINK_STATE_SEND:
      if (time_s >= uplink_tx_start + UPLINK_POWER_ON_TIME)
      {     
        sprintf (msg, "M4,%d,%d,%d,%d,%d,%d,%ld,%d", uplink_tx_since_boot, batt_v_min, temp1_min, temp1_max,temp2_min, temp2_max, vfs_vol_l, vfs_temp1_min);
        sbd_init(); // initialize Short Burst Data library
        sbd_send_msg (msg);
        uplink_state = UPLINK_STATE_SEND_WAIT;
      }
      break;

    case UPLINK_STATE_SEND_WAIT:
      /* Update SBD send msg */
      return_code = sbd_auto_send(time_s);

      /* check the return code */
      if (return_code != SBD_WAIT)
      {
        if (return_code == SBD_OK)
        {
          debug_serial_tx ("Uplink: Success");
          uplink_power (false);
          uplink_state = UPLINK_STATE_IDLE;
          uplink_tx_since_boot++;
        }
        else
        {
          if (time_s < uplink_tx_start + UPLINK_TX_MAX_TIME)
          {
            debug_serial_tx ("Uplink: Retry");
            uplink_state = UPLINK_STATE_SEND;
          }
          else
          {
            debug_serial_tx ("Uplink: Abort");
            led_blink(3, LED_SIGNAL);
            uplink_power (false);
            uplink_state = UPLINK_STATE_IDLE;
          }
        }
      }
      break;
  }
}
#endif

#ifdef UPLINK_GSM_SIM800
/***************************************************************************/

void uplink_init()
{
}
/***************************************************************************/
void uplink_tx()
{
}
/***************************************************************************/
void uplink_update()
{
}
#endif
/***************************************************************************/
void temp_reset()
{
  temp1_min = TEMP_MIN_RESET;
  temp1_max = TEMP_MAX_RESET;
  temp2_min = TEMP_MIN_RESET;
  temp2_max = TEMP_MAX_RESET;
}
/***************************************************************************/
void temp_init()
{
  sensors.begin(); // Start up the dallas sensor library
  temp_reset();
  next_temp_meas = millis() / 1000;
}
/***************************************************************************/
void temp_update()
{
  if (time_ms >= next_temp_meas)
  {
    /* update the temperature */
    sensors.requestTemperatures(); // Send the command to get temperature readings
    temp = (sensors.getTempCByIndex(0) * 10); /* read sensor id 0 */
    if (temp < temp1_min)
      temp1_min = temp;
    if (temp > temp1_max && temp < 850) /* < 850 to prevent error from dallas sensor */ 
      temp1_max = temp;

    #ifdef DEBUG_ENABLE
      sprintf (debug_serial_s, "Temp1: %d", temp);
      debug_serial_tx (debug_serial_s);
    #endif

    temp = (sensors.getTempCByIndex(1) * 10); /* read sensor id 1 */
    if (temp < temp2_min)
      temp2_min = temp;
    if (temp > temp2_max && temp < 850) /* < 850 to prevent error from dallas sensor */ 
      temp2_max = temp;

    #ifdef DEBUG_ENABLE
      sprintf (debug_serial_s, "Temp2: %d", temp);
      debug_serial_tx (debug_serial_s);
    #endif

    next_temp_meas += TEMP_MEAS_INTERVAL;
  }
}
/***************************************************************************/
void vfs_init()
{
  /* power on vfs flow sensor */
  #ifdef PCB_03_2018
    pinMode(PIN_VFS_PWR, OUTPUT);
    digitalWrite(PIN_VFS_PWR, HIGH);
  #endif

  vfs_vol_ul = 0;
  vfs_vol_l = 0;
  vfs_temp1_min = 1023;
  time_vfs_prev_meas = millis() - VFS_MEAS_INTERVAL;
}
/***************************************************************************/
void vfs_meas()
{
  /* read vfs flow sensor */
  vfs_flow_adc = analogRead(A1);
  if (vfs_flow_adc < VFS_FLOW_ADC_THRESHOLD_LOW)
    vfs_flow_adc = 0;
  else if (vfs_flow_adc > VFS_FLOW_ADC_THRESHOLD_HIGH)
    vfs_flow_adc = VFS_FLOW_ADC_THRESHOLD_HIGH;

  if (vfs_flow_adc > 0)
  {
    vfs_flow_ul_m = vfs_flow_adc * VFS_FLOW_A + VFS_FLOW_B;
    vfs_vol_meas_ul =  vfs_flow_ul_m / 60 * (time_ms - time_vfs_prev_meas) / 1000;
    vfs_vol_ul += vfs_vol_meas_ul;
    while (vfs_vol_ul > 1000000)
    {
      vfs_vol_l += 1;
      vfs_vol_ul -= 1000000;
    }
  }
  else
  {
    vfs_flow_ul_m = 0;
  }

  /* read vfs temp sensor */
  vfs_temp_adc = analogRead(A2);
  vfs_temp_deg = vfs_temp_adc * VFS_TEMP_A + VFS_TEMP_B;
  vfs_temp_deg /= 100000;
  if (vfs_temp_deg < vfs_temp1_min)
    vfs_temp1_min = vfs_temp_deg;

  /* update measurement time variable */
  time_vfs_prev_meas = time_ms;
}
/***************************************************************************/
void vfs_update()
{
  /* update the time */
  if (time_ms >= time_vfs_prev_meas + VFS_MEAS_INTERVAL)
  {
    vfs_meas();
  }
}
/***************************************************************************/
/* SBD library serial tx callback function */
void sbd_serial_tx(char *s)
{
  uplinkSerial->print (s);
}
/***************************************************************************/
void sbd_debug_serial_tx(char *text)
{
  #ifdef DEBUG_ENABLE
    debugSerial->begin(57600);
    debugSerial->print (time_s);
    debugSerial->print (" ");
    debugSerial->println(text);
    debugSerial->end();
  #endif
}
/***************************************************************************/
void setup()
{
  debug_init(); /* initialize serial debug */
  led_init(); /* initialize LED */

  #ifdef DEBUG_ENABLE
    debugSerial->begin(57600);
    debugSerial->println("");
    debugSerial->println("EWB Monitor (MONITOR)");
    debugSerial->println(FW_VERSION);
    debugSerial->end();
  #endif

  dip_switch_init(); /* initialize dip_switch */
  monitor_reset_init(); /* initialize the monitor controller reset */
  volt_init(); /* initialize the battery voltage measurement */
  temp_init(); /* initialize the temperature measurement */
  vfs_init(); /* initialize vfs flow sensor measurement */
  uplink_init(); /* initialize the uplink module */
}
/***************************************************************************/
void loop()
{
  time_ms = millis();  /* get the updated time [ms] */
  time_s = time_ms / 1000; /* get the updated time [s] */

  monitor_reset_update(); /* check if time to reset this monitor controller */
  led_update(); /* update the LED */
  dip_switch_update(); /* update the dip_switch status */
  volt_update(); /* update battery voltage measurement */
  temp_update(); /* update temperature measurement */
  vfs_update(); /* update fs flow sensor measurement */
  uplink_update(); /* update uplink module state machine */

  /* sleep for 15 ms to save power but keep TIMER0 on to preserve millis() */
  if (uplink_state == UPLINK_STATE_IDLE)
  {
    LowPower.idle(SLEEP_15MS, ADC_OFF, TIMER2_OFF, TIMER1_OFF, TIMER0_ON, SPI_OFF, USART0_OFF, TWI_OFF);
  }
}
/***************************************************************************/


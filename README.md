# EWB Monitor pilot project

Open source software and hardware concerning the project EWB Monitor pilot project conducted 2018-2019 in collaboration by Engineers Without Borders (EWB) - Denmark and EWB - Sierra Leone.

Please see license descriptions in the respective directories.


Contact information
-----------------------------------
Engineers Without Borders - Denmark
Att: Kjeld Jensen, MSc, PhD
Faculty of Engineering
University of Southern Denmark
kj@iug.dk
kjen@mmmi.sdu.dk
http://sdu.dk/staff/kjen

